# README #

This repo contains only the theme and minimum essential plugins and themes for wild-about-adventure.co.uk.

## Setting up wordpress

To install the remaining files so you have a functional wordpress install please use `wp core download`, then run through the default wordpress install and enable the wild about adventure theme.

## Working with the theme

The theme is built on `roots sage`, a starter theme for wordpress. https://roots.io/sage/, as such there are some dependancies that need to be installed before working with the theme.

First install `bower` (`brew install bower`).

Then `cd` to `wp-content/themes/wild-about-adventure/`

Then `bower install` to install gulp the project dependancies.

Then run `gulp` to compile assets, scss, images, etc.
