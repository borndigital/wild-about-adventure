<?php if( have_rows('page_and_menu_content') ): ?>
  <div class="adventures-main-content">
    <?php while( have_rows('page_and_menu_content') ): the_row();

      $left_image = get_sub_field('image_left');
      $right_image = get_sub_field('image_right');
      $title_id = str_replace(' ', '-', strtolower(get_sub_field('title')));

      ?>
      <div class="adventures-main-content-item">
        <div class="section-title-wrap inner">
          <h2 id="_<?php echo $title_id; ?>" class="adventures-main-content-item-title hidden-md hidden-lg section-title inner">
            <?php the_sub_field('title'); ?>
          </h2>
        </div>
        <div class="row vertical-align-center-md section-main-content inner">
          <?php if ( ! empty( $left_image ) ) { ?>
            <div class="image-wrap-col col-sm-12 col-lg-4 visible-md-block visible-lg-block">
              <img src="<?php echo $left_image['url']; ?>" alt="<?php echo $left_image['alt'] ?>" width="400" class="adventure-main-content-left-image"/>
            </div>
          <?php } ?>
          <div class="col-sm-12 <?php if ( ! empty( $left_image ) || ! empty( $right_image ) ) { echo "col-lg-8"; } else { echo "col-lg-12"; } ?>">
            <div class="adventures-main-content-item-wrap <?php if ( ! empty( $left_image ) ) { echo "left"; } ?> <?php if ( ! empty( $right_image ) ) { echo "right"; } ?>">
              <h2 id="<?php echo $title_id; ?>" class="adventures-main-content-item-title visible-md-block visible-lg-block">
                <?php the_sub_field('title'); ?>
              </h2>
                <p class="adventures-main-content-item-description "><?php the_sub_field('description'); ?></p>
            </div>
          </div>
          <?php if ( ! empty( $right_image ) ) { ?>
            <div class="image-wrap-col col-sm-12 col-lg-4">
              <img src="<?php echo $right_image['url']; ?>" alt="<?php echo $right_image['alt'] ?>" width="400" class="adventure-main-content-right-image"/>
            </div>
          <?php } ?>
        </div>
      </div>

    <?php endwhile; ?>
  </div>
<?php endif; ?>
