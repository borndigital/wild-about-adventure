<div class="feedback-block">
  <?php if( have_rows('feedback') ): ?>
    <h3>
      Our Feedback
    </h3>
  <?php  endif; ?>
  <ul class="feedback-list">
    <?php
    if( have_rows('feedback') ):
      while ( have_rows('feedback') ) : the_row(); ?>
        <li class="feedback-item">
          <div class="feedback-item-inner">
            <p class="large">
              <?php echo the_sub_field('feedback_text'); ?>
            </p>
            <span class="author">
              <?php echo the_sub_field('feedback_author'); ?> -
            </span>
            <span class="author-details">
              <?php echo the_sub_field('feedback_author_extra'); ?>
            </span>
          </div>
        </li>
        <br>
      <?php endwhile;
    endif; ?>
  </ul>
</div>
