<div class="header-nav-drop-down adventure hidden" id="our-adventures">
  <div class="container wrap">
    <?php if( have_rows('adventure_types', 'option') ): ?>
      <ul class="adventure-category-list">
          <?php while( have_rows('adventure_types', 'option') ): the_row();
            $image = get_sub_field('image', 'options');
            $title = get_sub_field('title', 'options');
            $link = get_sub_field('link', 'options') ?: '#';
            $lastPath = basename(parse_url($link, PHP_URL_PATH));
          ?>

            <li class="adventure-category-list-item">
              <a href="<?php echo $link; ?>" class="adventure-category-list-item-link">
                <img src="<?php echo $image; ?>" alt="<?php echo $image; ?>" height="150" class="adventure-category-list-item-img"/>
                <span class="adventure-category-list-item-title">
                  <?php echo $title; ?>
                </span>
                <div class="adventure-category-list-item-overlay <?php echo $lastPath; ?>">
                  <span class="more sketch-line-after">
                    Find out more
                  </span>
                </div>
              </a>
            </li>
          <?php endwhile; ?>
        </ul>
    <?php endif; ?>
  </div>
</div>
