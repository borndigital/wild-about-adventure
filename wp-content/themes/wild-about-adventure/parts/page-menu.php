<?php if( have_rows('page_and_menu_content') ): ?>
  <ul class="adventures-sub-menu angle-right-after">
    <h4 class="adventures-sub-menu-title">
      <?php echo get_the_title(); ?>
    </h4>
    <?php while( have_rows('page_and_menu_content') ): the_row();
      $title_id = str_replace(' ', '-', strtolower(get_sub_field('title')));
    ?>
      <li class="adventures-sub-menu-item">
        <a href="#<?php echo $title_id; ?>" class="adventures-sub-menu-link">
          <?php the_sub_field('title'); ?>
        </a>
      </li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>
<div class="angle-right-after angle-overlay"></div>
<div class="adventure-sub-menu-link-block">
  <a href="<?php the_field('link_block_link_after_menu'); ?>">
    <div class="image-wrapper">
      <img src="<?php the_field('link_block_image_after_menu'); ?>" alt="<?php the_field('link_block_title_after_menu'); ?>" class="adventure-sub-menu-after-bg-image ">
    </div>
    <div class="adventure-sub-menu-after-overlay">
      <h3 class="margin-clear adventure-sub-menu-title-text">
        <?php the_field('link_block_title_after_menu'); ?>
      </h3>
      <span class="adventure-sub-menu-link-text sketch-line-after">
        <?php the_field('link_block_link_title_after_menu'); ?>
      </span>
    </div>
  </a>
</div>
