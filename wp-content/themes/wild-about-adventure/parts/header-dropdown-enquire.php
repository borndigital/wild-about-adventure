<div class="header-nav-drop-down enquire-drop-down-menu hidden" id="enquire-now-toggle">
  <div class="container wrap">
    <div class="row">
      <div class="col-sm-6 col-md-8">
      <div class="enquiry-form-not-hidden">
          <div class="form-title">
            Tell us a little about the adventure you're after...
          </div>
          <span class="form-text">
            I am looking for
          </span>
          <fieldset>
            <label class="sr-only">Select activity type</label>
            <select class="activity-type input-type-dotted-underline" id="hidden_input_5_9">
              <?php if( have_rows('adventure_types', 'option') ): ?>
                <?php while( have_rows('adventure_types', 'option') ): the_row(); ?>
                  <?php $title = get_sub_field('title', 'options'); ?>
                  <option class="activity-type-option"><?php echo $title; ?></option>
                <?php endwhile; ?>
              <?php endif; ?>
            </select>
          </fieldset>
          <span class="form-text">
            on
          </span>
          <input type="text" id="datepicker" class="activity-date input-type-dotted-underline" value="">
          <span class="form-text">
            for
          </span>
          <input type="number" class="activity-num-of-people input-type-dotted-underline input-small" id="hidden_input_5_11" value="0">
          <span class="form-text">
            people.
          </span>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="enquiry-form">
          <?php
            $id_or_title = 5;
            $display_title = false;
            $display_description = false;
            $display_inactive = false;
            $field_values = null;
            $ajax = true;
            $tabindex = 1;
            $echo = true;
            echo gravity_form($id_or_title, $display_title, $display_description, $display_inactive, $field_values, $ajax, $tabindex, $echo);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
