<?php if( have_rows('page_and_menu_content') ): ?>
  <ul class="adventures-sub-menu angle-right-after">
    <h4 class="adventures-sub-menu-title">
      <?php echo get_the_title(); ?>
    </h4>
    <?php while( have_rows('page_and_menu_content') ): the_row();
      $title_id = str_replace(' ', '-', strtolower(get_sub_field('title')));
    ?>
      <li class="adventures-sub-menu-item">
        <a href="#_<?php echo $title_id; ?>" class="adventures-sub-menu-link">
          <?php the_sub_field('title'); ?>
        </a>
      </li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>
