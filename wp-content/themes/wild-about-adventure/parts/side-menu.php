<?php
  $current_page = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $current = $post->ID;
  $parent = $post->post_parent;
  $grandparent_get = get_post($parent);
  $grandparent = $grandparent_get->post_parent;
?>
<?php if( have_rows('side_menu_content') ): ?>
  <ul class="adventures-sub-menu angle-right-after">
    <h4 class="adventures-sub-menu-title single">
      <?php if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)) {echo get_the_title($grandparent); }else {echo get_the_title($parent); }?>
    </h4>
    <?php while( have_rows('side_menu_content') ): the_row();
    ?>
      <li class="adventures-sub-menu-item  <?php if ( $current_page == get_sub_field('link') ) { echo "active";} ?>">
        <a href="<?php the_sub_field('link'); ?>" class="adventures-sub-menu-link">
          <?php the_sub_field('title'); ?>
        </a>
      </li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>
<div class="angle-right-after angle-overlay"></div>
<div class="adventure-sub-menu-link-block padding-bottom-large">
  <a href="<?php the_field('link_block_link_after_menu'); ?>">
    <div class="image-wrapper">
      <img src="<?php the_field('link_block_image_after_menu'); ?>" alt="<?php the_field('link_block_title_after_menu'); ?>" class="adventure-sub-menu-after-bg-image ">
    </div>
    <div class="adventure-sub-menu-after-overlay">
      <h3 class="margin-clear adventure-sub-menu-title-text">
        <?php the_field('link_block_title_after_menu'); ?>
      </h3>
      <span class="adventure-sub-menu-link-text sketch-line-after">
        <?php the_field('link_block_link_title_after_menu'); ?>
      </span>
    </div>
  </a>
</div>
