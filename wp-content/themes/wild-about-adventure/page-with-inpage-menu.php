<?php
/*
Template Name: Page with inpage menu
*/
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-with-menu'); ?>
<?php endwhile; ?>
