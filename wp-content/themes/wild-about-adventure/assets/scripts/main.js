/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire("common");

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, "_").split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, "finalize");
      });

      // Fire common finalize JS
      UTIL.fire("common", "finalize");
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

(function($) {
  function applyEventFilters() {
    $(".waa-event-filters [data-event-category]").each(function() {
      var filter = $(this).data().eventCategory;
      var active = $(this).hasClass("active");
      var items = $(".tribe-events-month").find("[data-event-category='" + filter + "']");
      items.toggleClass("active", active);
    });
  }

  function startWait() {
    $('body').addClass('loading');
  }

  function finishWait() {
    $('body').removeClass('loading');
  }

  function toggleEventType() {
    $(".waa-event-filters [data-event-category]").removeClass("active");
    var str = $(this).val().replace(/\s+/g, '-').toLowerCase();

    $("option[data-event-category='" + str + "']").addClass("active");

    applyEventFilters();
  }

  function showAllEventTypes() {
    $(".waa-event-filters [data-event-category]").addClass("active");
    applyEventFilters();
  }

  function updateCalendarCurrentMonth() {
    // get the current month
    // add a current class to the relevant calendar link
    $('.tribe-events-sub-nav a').removeClass('current');
    $('.tribe-events-sub-nav a[data-month="' + window.currentMonthUrl + '"]').addClass('current');
  }

  function loadMonth(url) {
    var deferred = jQuery.Deferred();
    startWait();
    $.get(url, function(response) {
      var html = $(response);
      var calendar = html.find(".tribe-events-calendar");
      var date = html.find(".tribe-events-page-title").text();
      var prev = html.find(".tribe-events-sub-nav-block .arrow-prev").attr("href");
      var next = html.find(".tribe-events-sub-nav-block .arrow-next").attr("href");
      $(".tribe-events-sub-nav-block .arrow-prev").attr("href", prev);
      $(".tribe-events-sub-nav-block .arrow-next").attr("href", next);
      $(".tribe-events-calendar").html(calendar);
      $(".tribe-events-page-title").text(date);
      updateCalendarCurrentMonth();
      applyEventFilters();
      finishWait();
      deferred.resolve();
    });
    return deferred;
  }

  // load the calendar via ajax when we change the month
  $("body").on("click", ".tribe-events-sub-nav-block [data-month]", function(e) {
    e.preventDefault();
    var url = $(this).attr("href");
    window.currentMonthUrl = url.replace(new RegExp('.+events/'), '').replace('/', '');
    loadMonth(url);
    return false;
  });

  var $bookingModal = $("#booking-modal");
  var $pageModal = $("#page-modal");

  function loadCalendar(url) {
    var deferred = jQuery.Deferred();
    startWait();
    $.get(url, function(response) {
      var html = $(response);
      var calendar = html.find(".tribe-events-month");
      $("#events-side .inner").html(calendar);
      updateCalendarCurrentMonth();
      showAllEventTypes();
      finishWait();
      deferred.resolve();
    });
    return deferred;
  }

  function loadDay(url) {
    var deferred = jQuery.Deferred();
    startWait();
    $.get(url, function(response) {
      var html = $(response);
      var info = html.find("#tribe-events-content");
      var form = html.find(".booking-form");

      $(".event-info").html(info);
      $(".event-form").html(form);
      finishWait();

      deferred.resolve();
    });
    return deferred;
  }

  function showBookingForm() {
    $(".initial-bookings-form").removeClass("hidden");
    $(".event-info").addClass("hidden");
    $(".event-form").addClass("hidden");
  }

  function showEventInfo() {
    $(".initial-bookings-form").addClass("hidden");
    $(".event-info").removeClass("hidden");
    $(".event-form").addClass("hidden");
  }

  function showEventForm() {
    $(".initial-bookings-form").addClass("hidden");
    $(".event-info").addClass("hidden");
    $(".event-form").removeClass("hidden");
  }

  function getCurrentMonthUrl() {
    var today = new Date();
    var mm = today.getMonth() + 1; // January is 0
    var yyyy = today.getFullYear();

    if (mm < 10) {
      mm = "0" + mm;
    }

    return yyyy + "-" + mm;
  }

  $("body").on("click","[href*='/events/']", "[href*='events/month'], [href*='/bookings/']", function(e) {
    e.preventDefault();

    startWait();
    var url = $(this).attr("href");
    url = url.replace('/bookings/', '/events/month');
    window.currentMonthUrl = getCurrentMonthUrl();
    deferredCalendar = loadCalendar(url);

    $.when(deferredCalendar).done(function() {
      $bookingModal.modal();
      $bookingModal.modal("show");
      finishWait();
    });

    return false;
  });

  $("body").on("click", ".day-number a", function(e) {
    e.preventDefault();
    startWait();

    loadDay($(this).attr("href"))
      .then(function() {
        showEventInfo();
        finishWait();
      });

    return false;
  });

  $("body").on("click", ".book-now", function(e) {
    e.preventDefault();
    showEventForm();
    return false;
  });

  $("#booking-modal").on("hidden.bs.modal", function (e) {
    showBookingForm();
  });

  function padLeft(nr, n, str) {
    return Array(n-String(nr).length+1).join(str||'0')+nr;
  }

  function hasGallery(page) {
    return page.hasOwnProperty("custom_fields") && page.custom_fields.hasOwnProperty("gallery") && page.custom_fields.gallery[0] !== "";
  }

  function hasAttachments(page) {
    return page.hasOwnProperty("attachments") && page.attachments.length;
  }

  function shouldShowGallery(page) {
    return hasGallery(page) && hasAttachments(page);
  }

  function hasInstructors(page) {
    return page.hasOwnProperty("custom_fields") && page.custom_fields.hasOwnProperty("instructors");
  }

  function attachmentImages(page) {
    var html = '';
    for (var i = page.attachments.length - 1; i >= 0; i--) {
      html += '<img src="' + page.attachments[i].url + '" title="' + page.attachments[i].title + '" />';
    }
    return html;
  }

  function instructorMarkup(instructor) {
    var name = '<div class="instructor-name">' + instructor.name + '</div>';
    var title = '<div class="instructor-title">' + instructor.title + '</div>';
    var description = '<div class="instructor-description">' + instructor.description + '</div>';
    var image = '<div class="instructor-image"><img src="' + instructor.image + '" alt="' + instructor.title + '" /></div>';

    var html = '<div class="row">';
    html += '<div class="col-sm-12">' + name + '</div>';
    html += '<div class="col-sm-5">' + image + '</div>';
    html += '<div class="col-sm-7">' + title + description + '</div>';
    html += '</div>';
    return html;
  }

  function instructorsList(instructors) {
    var html = '<div class="instructors">';
    for (var i = instructors.length - 1; i >= 0; i--) {
      html += instructorMarkup(instructors[i]);
    }
    return html + '</div>';
  }

  function hasForm(page) {
    return page.hasOwnProperty("form");
  }

  function loadPage(url) {
    var deferred = jQuery.Deferred();
    $.get(url + "?json=1", function(response) {
      $pageModal.find(".modal-body").html("<h2>" + response.page.title + "</h2>");
      $pageModal.find(".modal-body").append(response.page.content);
      $pageModal.find(".modal-right").html("");
      $pageModal.find(".modal-content").attr("data-page", response.page.slug);

      if (shouldShowGallery(response.page)) {
        var attachmentHtml = attachmentImages(response.page);
        $pageModal.find(".modal-right").html(attachmentHtml);
      }

      if (hasInstructors(response.page)) {
        var firstTwoInstructors = response.page.custom_fields.instructors.splice(0, 2);
        var remainingInstructors = response.page.custom_fields.instructors;
        var InstructorsLeftColumn = instructorsList(firstTwoInstructors);
        var InstructorsRightColumn = instructorsList(remainingInstructors);

        $pageModal.find(".modal-body").append(InstructorsLeftColumn);
        $pageModal.find(".modal-right").html(InstructorsRightColumn);
      }

      if (hasForm(response.page)) {
        var formHtml = response.page.form;
        $pageModal.find(".modal-right").html(formHtml);
      }

      deferred.resolve();
    });
    return deferred;
  }

  $("body").on("click", ".accreditations", function(e) {
    e.preventDefault();

    var deferred = loadPage($(this).attr("href"));

    $.when(deferred).done(function() {
      $pageModal.modal();
      $pageModal.modal("show");
    });

    return false;
  });

  function wrapFirstWord($el) {
    var html = $el.html().trim();
    var word = html.substr(0, html.indexOf(" "));
    var rest = html.substr(html.indexOf(" "));
    $el.html(rest).prepend($("<span/>").html(word).addClass("first-word"));
  }

  if ($('.new-skills').length) {
    wrapFirstWord($('.new-skills .caption'));
  }

  function Cycling(window, el) {
    var instance = this;
    var $el = $(el);
    var delay = parseInt($el.data().delay);
    var activeClass = "hover";
    var interval = 6000;

    instance.show = function() {
      $el.addClass(activeClass);
    };

    instance.hide = function() {
      $el.removeClass(activeClass);
    };

    instance.cycle = function() {
      if ($el.hasClass(activeClass)) {
        instance.hide();
      } else {
        instance.show();
      }
    };

    function init() {
      window.setTimeout(function() {
        instance.cycle();
        instance.interval = window.setInterval(instance.cycle, interval);
      }, delay);
    }

    init();

    return instance;
  }

  if ($('.with-caption').length) {
    $('.with-caption[data-delay]').each(function() {
      window.foo = new Cycling(window, this);
    });
  }

  $("#enquire-now-trigger").on('click', function () {
    //Show nav dropdown
    $("#enquire-now-toggle").toggleClass('hidden');
    $(this).parent().toggleClass('active');

    //Hide other open menu items
    $("#menu-top-menu .menu-item").each(function() {
      $(this).removeClass('active');
      $('#our-adventures').addClass('hidden');
    });
  });

  // desktop menu
  $("#menu-top-menu .menu-item").on('click', function () {
    $("#menu-top-menu .menu-item").each(function() {
      $(this).removeClass('active');
    });
    //Show nav dropdown
    if ($(this).find('a').attr('href') === "#our-adventures") {
      $("#our-adventures").toggleClass('hidden');
      $(this).toggleClass('active');
    }

    //Hide other open menu items
    $('.enquire-now-wrap').removeClass('active');
    $('#enquire-now-toggle').addClass('hidden');
  });

  // mobile menu
  $('.mobile-menu-nav .menu-item').on('click', function () {
    if ($(this).find('a').attr('href') === "#our-adventures") {

      $(this).find(".sub-menu").toggleClass('active');
    }
  });


  //init datepicker
  $("#datepicker").datepicker({
    dateFormat: 'dd/mm/yy',
    showAnim: 'slideDown'
  });

  $("#hidden_input_5_9").selectmenu();

  $( ".activity-type" ).on( "selectmenuchange", function( event, ui ) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    $("#input_5_9").val(valueSelected);
    $('.ui-selectmenu-text').addClass('active');
    $('#hidden_input_5_9').addClass('active');
  } );


  $("#input_5_9").on('change', function() {
    $(this).addClass('active');
  });

  $('.enquiry-form-not-hidden .ui-selectmenu-text').text('Select a course');


  if(($("#datepicker").length > 0)) {
    $("#datepicker").val('Select a date');
  }


  $("#datepicker").on('change', function() {
    $("#input_5_10").val($(this).val());
    $(this).addClass('active');
  });

  $("#hidden_input_5_11").on('change', function() {
    $("#input_5_11").val($(this).val());
    $(this).addClass('active');
  });

  if($('.adventure-carousel').length > 0) {
    $('.adventure-carousel').slick({
      arrows: false,
    });
  }

  $('.adventure-carousel-right').on('click', function() {
    $('.adventure-carousel').slick('slickNext');
  });

  $('.adventure-carousel-left').on('click', function() {
    $('.adventure-carousel').slick('slickPrev');
  });

  $('#toggle-mobile-menu').on('click', function () {
    $('.mobile-menu-wrap').toggleClass('hidden');
    $(this).toggleClass('active');
  });

  $('.section-title-wrap').on('click', function() {
    $(this).toggleClass('active-content');
    $(this).siblings('.section-main-content').toggleClass('active-content');
  });

})(jQuery);
