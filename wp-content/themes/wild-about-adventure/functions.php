<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function is_ajax() {
  return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

// Custom function to output the months of the current year for the events calendar
function tribe_events_next_twelve_months() {
  $output = '';
  $tribe_ecp = Tribe__Events__Main::instance();
  $date = strftime('%Y-01');
  $months = array();

  for ($i=0; $i < 12; $i++) {
    $url = $tribe_ecp->getLink( 'month', $date, null );
    $text = date('M', strtotime($date));

    $output .= '<li>';
    $output .= '<a data-month="' . $date . '" href="' . esc_url( $url ) . '" rel="next">' . $text . ' </span></a>';
    $output .= '</li>';

    // increment the date
    $date = $tribe_ecp->nextMonth($date);
  }

  echo $output;
}

function waa_events_title($str) {
  if (tribe_is_month()) {
    $str = str_replace('Events for ', '', $str);
    $str = date('F Y', strtotime($str));
  }
  return $str;
}
add_filter( 'tribe_get_events_title', 'waa_events_title', 10, 3 );

function waa_first_event_category_for_day($day) {
  $category = '';

  if ($day['events']->have_posts()) {
    $first_event = waa_first_event_for_day($day);
    $terms = get_the_terms($first_event->ID, 'tribe_events_cat');

    if (empty($terms) == false) {
      $category = $terms[0]->slug;
    }
  }

  return $category;
}

function waa_first_event_for_day($day) {
  $event = false;
  if ($day['events']->have_posts()) {
    $event = $day['events']->posts[0];
  }
  return $event;
}

// function waa_tribe_events_event_schedule_details($html, $event_id, $before, $after) {
//   $html = substr($html, 0, strpos($html, '<div class="recurringinfo">'));
//   return $html;
// }
// add_filter( 'tribe_events_event_schedule_details', 'waa_tribe_events_event_schedule_details', 10, 3 );

function waa_event_date() {
  $date = tribe_events_event_schedule_details($event_id, '', '');
  return $date;
}

// Kill the events calendar js, it's nothing but trouble!
function dequeue_tribe_events_scripts_and_styles() {
  $scripts = array(
    'tribe-events-pro-geoloc',
    'tribe-events-list',
    'tribe-events-pro',
    'tribe-events-calendar-script',
    'tribe-events-bar',
    'tribe-events-bootstrap-datepicker',
    'tribe-placeholder',
    'tribe-events-jquery-resize',
    'tribe-events-ajax-calendar',
    'the-events-calendar',
    'tribe-this-week',
  );
  foreach ($scripts as $script) {
    wp_dequeue_script($script);
  }

  $styles = array(
    'tribe-events-bootstrap-datepicker-css',
    'tribe-events-calendar-style',
    'tribe-events-custom-jquery-styles',
    'tribe-events-calendar-style',
    'tribe-events-calendar-pro-style',
    'tribe_events-widget-this-week-pro-style',
  );
  foreach ($styles as $style) {
    wp_dequeue_style($style);
  }
}
add_action('wp_enqueue_scripts', 'dequeue_tribe_events_scripts_and_styles', 100 );

function waa_register_footer_menu() {
  register_nav_menus(array(
    'footer_navigation' => 'Footer',
  ));
  remove_editor_styles();
}
add_action('init', 'waa_register_footer_menu');

function waa_json_api_instructors($data) {
  $custom_fields = $data['page']->custom_fields;

  if ($custom_fields->instructors) {
    $instructors = array();
    $instructors_count = intval($custom_fields->instructors[0]);
    $instructor_fields = array('name', 'title', 'description', 'image');

    for ($i = 0; $i < $instructors_count; $i++) {
      $instructor = array();

      foreach ($instructor_fields as $field) {
        $key = 'instructors_' . $i . '_' . $field;
        $instructor[$field] = $custom_fields->{$key}[0];

        if ($field == 'image') {
          $instructor[$field] = waa_get_image_url($instructor[$field]);
        }

        // remove this key from the custom fields
        unset($data['page']->custom_fields->{$key});
      }

      $instructors[] = $instructor;
    }

    // update
    $data['page']->custom_fields->instructors = $instructors;
  }

  return $data;
}
add_filter( 'json_api_encode', 'waa_json_api_instructors', 10, 1 );

function waa_json_api_contact($data) {
  if ($data['page']->slug == 'contact-us') {
    $id_or_title = 3;
    $display_title = true;
    $display_description = true;
    $display_inactive = false;
    $field_values = null;
    $ajax = true;
    $tabindex = 1;
    $echo = false;
    $form = gravity_form($id_or_title, $display_title, $display_description, $display_inactive, $field_values, $ajax, $tabindex, $echo);


    $data['page']->form = $form;
  }
  return $data;
}
add_filter( 'json_api_encode', 'waa_json_api_contact', 10, 1 );

function waa_get_image_url($id) {
  $id = intval($id);
  $url = wp_get_attachment_image_src($id);
  return $url[0];
}

function waa_tribe_events_before_html($html) {
  return '';
}
add_filter('tribe_events_before_html', 'waa_tribe_events_before_html', 10, 1);

function get_meta_boxes( $screen = null, $context = 'advanced' ) {
    global $wp_meta_boxes;

    if ( empty( $screen ) )
        $screen = get_current_screen();
    elseif ( is_string( $screen ) )
        $screen = convert_to_screen( $screen );

    $page = $screen->id;

    return $wp_meta_boxes[$page][$context];
}

// add a favicon to your
function waa_favicon() {
  echo '<link rel="shortcut icon" href="' . get_bloginfo('stylesheet_directory') . '/assets/images/favicon.png" />';
}
add_action('wp_head', 'waa_favicon');

add_filter('nav_menu_css_class' , 'active_nav_class' , 10 , 2);
function active_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}


if( function_exists('acf_add_options_page') ) {

  acf_add_options_page();

}

require_once( 'includes/acf_fields.php' );
require_once( 'includes/page-with-menu.php' );
require_once( 'includes/side-menu.php' );
