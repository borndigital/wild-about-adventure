<?php if ($_GET['thankyou-booking']): ?>
  <div class="alert alert-success">
    <h3 class="alert-title">Thanks for your booking!</h3>
    We will get in touch with you shortly. For any urgent enquiries call us on <strong>07966902530</strong>.
  </div>
<?php endif; ?>
<?php if ($_GET['thankyou']): ?>
  <div class="alert alert-success">
    <h3 class="alert-title">Thanks for contacting us!</h3>
    We will get in touch with you shortly. For any urgent enquiries call us on <strong>07966902530</strong>.
  </div>
<?php endif; ?>

<div class="row home-trellis">
  <div class="col-sm-6 text-right">
    <div class="row">
      <div class="col-sm-12">
        <?php
          $image = get_field('left_first_image');
          $title = get_field('left_first_image_title');
          $link = get_field('left_first_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption school-activities" data-delay="0">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center caption">
              <?php echo $title ?>
            </span>
            <span class="bottom find-out-more sketch-line-after hidden-xs hidden-sm hidden-xs">
              Find out more
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-push-3 col-sm-9">
        <?php
          $image = get_field('left_second_image');
          $title = get_field('left_second_image_title');
          $text = get_field('left_second_image_text');
          $link = get_field('left_second_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption with-description holiday-clubs" data-delay="2000">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center">
              <span class="caption">
                <?php echo $title; ?>
              </span>
              <span class="description hidden-xs hidden-sm hidden-md">
                <?php echo $text; ?>
              </span>
            </span>
            <span class="bottom find-out-more sketch-line-after hidden-xs hidden-sm">
              Find out more
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 visible-xs-block">
        <blockquote class="xl explore">
          <span class="thick">
            <?php the_field('left_statement_first_line'); ?>
          </span>
          <span class="thin">
            <?php the_field('left_statement_second_line'); ?>
          </span>
        </blockquote>
      </div>
    </div>
    <div class="row has-bottom-aligned row-bg-color-xs-half">
      <div class="col-xs-6 col-sm-12 col-md-4 bottom-aligned">
        <a href="<?php echo site_url('our-awards') ?>" class="square-box trophy-link">
          <span>Our awards</span>
        </a>
      </div>
      <div class="visible-xs-block visible-sm-block col-xs-6 hidden-sm">
        <a href="<?php echo site_url('events/month') ?>" class="square-box calendar-link" data-delay="5000">
          <span>Bookings</span>
        </a>
      </div>
      <div class="hidden-xs hidden-sm col-md-8 col-lg-7 bottom-aligned">
        <div class="box square-box box-quote with-cite">
          <blockquote>
            <p class="center said">
              <?php the_field('left_quote'); ?>
            </p>
            <p class="bottom cite">
              <?php the_field('left_quote_from'); ?>
            </p>
          </blockquote>
        </div>
      </div>
    </div>
    <div class="visible-xs-block col-xs-12 bottom-aligned row-bg-color-xs">
        <div class="box square-box box-quote with-cite box-quote-xs">
          <blockquote>
            <p class="center said">
              <?php the_field('left_quote'); ?>
            </p>
            <p class="bottom cite">
              <?php the_field('left_quote_from'); ?>
            </p>
          </blockquote>
        </div>
    </div>
    <div class="row">
      <div class="hidden-xs col-sm-12 margin-bottom-small">
        <?php
          $image = get_field('left_third_image');
          $title = get_field('left_third_image_title');
          $link = get_field('left_third_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption get-mucky" data-delay="7000">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center caption">
              <?php echo $title; ?>
            </span>
            <span class="bottom find-out-more sketch-line-after hidden-xs hidden-sm">
              Find out more
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 hidden-xs">
        <blockquote class="xl">
          <span class="thick">
            <?php the_field('left_statement_first_line'); ?>
          </span>
          <span class="thin">
            <?php the_field('left_statement_second_line'); ?>
          </span>
        </blockquote>
      </div>
    </div>
    <div class="row">
      <div class="hidden-xs col-sm-12">
        <?php
          $image = get_field('left_fourth_image');
          $title = get_field('left_fourth_image_title');
          $link = get_field('left_fourth_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption challenge-yourself" data-delay="6000">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center caption">
              <?php echo $title; ?>
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="row">
      <div class="col-sm-12">
        <?php
          $image = get_field('right_first_image');
          $title = get_field('right_first_image_title');
          $text = get_field('right_first_image_text');
          $link = get_field('right_first_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption with-description team-building" data-delay="1000class="home-trellis-image"">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image" />
            <span class="center">
              <span class="caption">
                <?php echo $title; ?>
              </span>
              <span class="description hidden-xs">
                <?php echo $text; ?>
              </span>
            </span>
            <span class="bottom find-out-more sketch-line-after hidden-xs hidden-sm">
              Find out more
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row hidden-xs hidden-sm">
      <div class="col-md-6">
        <div class="box square-box box-quote">
          <blockquote>
            <div class="center said">
              <span class="quot">&#8220;</span>
              <?php the_field('right_quote'); ?>
            </div>
          </blockquote>
        </div>
      </div>
      <div class="hidden-xs hidden-sm col-md-4">
        <a href="<?php echo site_url('events/month') ?>" class="square-box calendar-link" data-delay="5000">
          <span>Bookings</span>
        </a>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-sm-12">
        <blockquote class="xl">
          <span class="thin">
            <?php the_field('right_statement_first_line'); ?>
          </span>
          <span class="thick">
            <?php the_field('right_statement_second_line'); ?>
          </span>
        </blockquote>
      </div>
    </div>
    <div class="row">
      <div class="hidden-xs col-sm-9">
        <?php
          $image = get_field('right_second_image');
          $title = get_field('right_second_image_title');
          $link = get_field('right_second_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption experience-outdoors" data-delay="4000">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center caption">
              <?php echo $title; ?>
            </span>
            <span class="bottom find-out-more sketch-line-after hidden-xs hidden-sm">
              Find out more
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-md-6" style="">
        <?php
          $image = get_field('right_third_image');
          $title = get_field('right_third_image_title');
          $link = get_field('right_third_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption new-skills" data-delay="8000">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center caption">
              <?php echo $title; ?>
            </span>
          </a>
        <?php endif; ?>
      </div>
      <div class="col-md-4">
        <a href="<?php echo site_url('events/month') ?>" class="square-box calendar-link alt">
          <span>Bookings</span>
        </a>
      </div>
    </div>
    <div class="row">
      <div class="hidden-xs col-sm-12">
        <?php
          $image = get_field('right_fourth_image');
          $title = get_field('right_fourth_image_title');
          $link = get_field('right_fourth_image_link') ?: '#';
        ?>
        <?php if( !empty($image) ): ?>
          <a href="<?php echo $link; ?>" class="box with-caption make-new-friends" data-delay="0">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-trellis-image"/>
            <span class="center caption">
              <?php echo $title; ?>
            </span>
          </a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="padding-clear-xs col-sm-12 col-md-6 col-lg-6 margin-bottom-clear">
        <?php
          $title = get_field('right_green_box_text');
          $link = get_field('right_green_box_link') ?: '#';
        ?>
        <a href="<?php echo $link; ?>">
          <span class="square-box accreditations-link credit light-green" data-delay="5000">
            <span><?php echo $title; ?></span>
          </span>
        </a>
      </div>
      <div class="padding-clear-xs col-sm-12 col-md-8 col-lg-6 margin-bottom-clear">
        <?php
          $title = get_field('right_white_box_text');
          $link = get_field('right_white_box_link') ?: '#';
        ?>
        <a href="<?php echo $link; ?>" target="_blank">
          <span class="square-box accreditations-link support white" data-delay="5000">
            <span>Proud to support</span>
          </span>
        </a>
      </div>
    </div>
  </div>
</div>
