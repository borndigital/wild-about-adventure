<?php
/*
Template Name: Page with side menu
*/
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page-with-side-menu'); ?>
<?php endwhile; ?>
