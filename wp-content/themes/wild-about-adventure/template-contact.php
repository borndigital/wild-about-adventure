<?php
/**
 * Template Name: Contact template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="contact-padding">
    <?php get_template_part('templates/page', 'header'); ?>
    <div class="row contact-container">
      <div class="col-sm-7 col-md-6">
        <?php get_template_part('templates/content', 'contact'); ?>
      </div>
      <div class="col-sm-5 col-md-6">
        <?php get_template_part('templates/contact', 'form'); ?>
      </div>
    </div>
  </div>
<?php endwhile; ?>
