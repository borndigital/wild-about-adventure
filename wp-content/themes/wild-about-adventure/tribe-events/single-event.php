<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();
$event_date = waa_event_date();
$notices = tribe_the_notices(false);

// event status
$fully_booked = get_field('fully_booked') == true;
$event_has_passed = strpos($notices, 'This event has passed');

?>

<div id="tribe-events-content" class="tribe-events-single">

  <h2>
    <?php echo $event_date; ?> - £<?php echo tribe_get_cost(); ?>
  </h2>

  <?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>

  <?php while ( have_posts() ) :  the_post(); ?>
    <?php the_field('event_intro'); ?>
  <?php endwhile; ?>

  <p class="text-right">
    <?php if ($event_has_passed): ?>
      <span class="btn btn-primary" disabled>
        This event has passed
      </a>
    <?php elseif ($fully_booked): ?>
      <span class="btn btn-primary" disabled>
        This event is fully booked
      </a>
    <?php else: ?>
      <a href="#booking-form" class="btn btn-primary book-now">
        Book now
      </a>
    <?php endif; ?>
  </p>

</div><!-- #tribe-events-content -->

<?php if ($fully_booked == false): ?>
  <!-- Booking form for the event would go here -->
  <div id="booking-form" class="booking-form">
    <h1>
      <?php the_title(); ?> - <?php echo $event_date; ?> - £<?php echo tribe_get_cost(); ?>
    </h1>
    <p class="lead">
      <?php the_field('event_information'); ?>
    </p>

    <h3>
      Event booking
    </h3>
    <?php the_field('booking_information'); ?>
    <?php
      $id_or_title = 'Event booking';
      $display_title = false;
      $display_description = false;
      $display_inactive = false;
      $field_values = array(
        'event' => get_the_title() . ' - ' . $event_date,
        'price' => tribe_get_cost(),
      );
      $ajax = true;
      $tabindex = 1;
      $echo = true;
      echo gravity_form($id_or_title, $display_title, $display_description, $display_inactive, $field_values, $ajax, $tabindex, $echo);
    ?>
  </div>
<?php endif; ?>
