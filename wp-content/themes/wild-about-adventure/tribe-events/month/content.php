<?php
/**
 * Month View Content Template
 * The content template for the month view of events. This template is also used for
 * the response that is returned on month view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/content.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
} ?>

<div id="tribe-events-content" class="tribe-events-month">

  <!-- Month Title -->
  <?php do_action( 'tribe_events_before_the_title' ) ?>
  <h2 class="tribe-events-page-title"><?php tribe_events_title() ?></h2>
  <?php do_action( 'tribe_events_after_the_title' ) ?>

  <!-- Notices -->
  <?php tribe_the_notices() ?>

  <!-- Month Header -->
  <?php do_action( 'tribe_events_before_header' ) ?>
  <div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>

    <!-- Header Navigation -->
    <?php tribe_get_template_part( 'month/nav' ); ?>

  </div>
  <!-- #tribe-events-header -->
  <?php do_action( 'tribe_events_after_header' ) ?>

  <!-- Month Grid -->
  <?php tribe_get_template_part( 'month/loop', 'grid' ) ?>

  <!-- Month Footer -->
  <?php do_action( 'tribe_events_before_footer' ) ?>

  <!-- #tribe-events-footer -->
  <?php //do_action( 'tribe_events_after_footer' ) ?>

  <?php tribe_get_template_part( 'month/mobile' ); ?>
  <?php tribe_get_template_part( 'month/tooltip' ); ?>

</div><!-- #tribe-events-content -->

<?php if (is_ajax() == false) { ?>

<div class="waa-event-filters">
  <h2>
    Show me...
  </h2>
  <?php $terms = get_terms("tribe_events_cat"); ?>
  <ul class="row list-unstyled">
    <?php foreach ($terms as $term) { ?>
      <li class="col-sm-4">
        <span class="btn btn-default active" data-event-category="<?php echo $term->slug; ?>">
          <?php echo $term->name; ?>
        </span>
      </li>
    <?php } ?>
  </ul>
</div>

<?php } ?>
