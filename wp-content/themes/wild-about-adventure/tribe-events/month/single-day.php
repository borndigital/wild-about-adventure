<?php
/**
 * Month View Single Day
 * This file contains one day in the month grid
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/single-day.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

$day = tribe_events_get_current_month_day();
$events_label = ( 1 === $day['total_events'] ) ? tribe_get_event_label_singular() : tribe_get_event_label_plural();

$daydata = tribe_events_get_current_month_day();
$first_event_for_day = waa_first_event_for_day($daydata);
$show_link = $first_event_for_day ? true : false;
?>

<!-- Day Header -->
<div id="tribe-events-daynum-<?php echo $day['daynum-id'] ?>" class="day-number">
  <?php if ($show_link) { echo '<a href="' . get_permalink($first_event_for_day->ID) . '">'; } ?>
  <?php echo $day['daynum'] ?>
  <?php if ($show_link) { echo '</a>'; } ?>
</div>
