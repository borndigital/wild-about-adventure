<?php
/**
 * Month View Nav Template
 * This file loads the month view navigation.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/nav.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
} ?>

<?php do_action( 'tribe_events_before_nav' ) ?>
  <div class="tribe-events-sub-nav-block">
    <a href="<?php echo tribe_get_previous_month_link(); ?>" id="previous" class="arrow-prev" data-month="<?php echo tribe_get_previous_month_link(); ?>">
    </a>

    <a href="<?php echo tribe_get_next_month_link(); ?>" id="next" class="arrow-next" data-month="<?php echo tribe_get_next_month_link(); ?>">
    </a>
  </div>
<?php
do_action( 'tribe_events_after_nav' );
