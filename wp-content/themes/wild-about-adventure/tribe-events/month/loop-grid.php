<?php
$days_of_week = tribe_events_get_days_of_week('short');
$week = 0;
global $wp_locale;
?>

<div class="tribe-events-calendar-wrap">
  <?php do_action( 'tribe_events_before_the_grid' ) ?>
    <table class="tribe-events-calendar">
      <thead>
      <tr>
        <?php foreach ( $days_of_week as $day ) : ?>
          <th id="tribe-events-<?php echo esc_attr( strtolower( $day ) ); ?>" title="<?php echo esc_attr( $day ); ?>" data-day-abbr="<?php echo esc_attr( $wp_locale->get_weekday_abbrev( $day ) ); ?>">
            <?php echo $day ?>
          </th>
        <?php endforeach; ?>
      </tr>
      </thead>
      <tbody>
      <tr>
        <?php while ( tribe_events_have_month_days() ) : tribe_events_the_month_day(); ?>
        <?php if ( $week != tribe_events_get_current_week() ) : $week ++; ?>
      </tr>
      <tr>
        <?php endif; ?>

        <?php
        // Get data for this day within the loop.
        $daydata = tribe_events_get_current_month_day(); ?>

        <td class="<?php tribe_events_the_month_day_classes() ?> active"
          data-day="<?php echo esc_attr( isset( $daydata['daynum'] ) ? $daydata['date'] : '' ); ?>"
          data-tribejson='<?php echo tribe_events_template_data( null, array( 'date_name' => tribe_format_date( $daydata['date'], false ) ) ); ?>'
          data-event-category='<?php echo waa_first_event_category_for_day($daydata); ?>'
          >
          <?php tribe_get_template_part( 'month/single', 'day' ) ?>
        </td>
        <?php endwhile; ?>
      </tr>
      </tbody>
    </table><!-- .tribe-events-calendar -->
  <?php
  do_action( 'tribe_events_after_the_grid' );
  ?>
</div>
