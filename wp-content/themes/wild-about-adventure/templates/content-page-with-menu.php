<div class="row">
  <div class="section-title-wrap hidden-md hidden-lg active-content">
    <h3 class="section-title">
      In this section
    </h3>
  </div>
  <div class="page-menu-collapsable hidden-md hidden-lg padding-clear section-main-content active-content">
    <?php get_template_part('parts/mobile-page-menu'); ?>
  </div>
  <div class="col-md-8 adventure-page-wrap">
    <?php get_template_part('templates/page', 'header'); ?>
    <?php
      $description = get_field('page_summary_description');
      $images = get_field('page_summary_images');
    ?>
    <?php if (! empty($description)) : ?>
      <p><?php echo $description; ?></p>
    <?php endif; ?>
    <?php if( $images ): ?>
        <div class="adventure-carousel-wrapper">
          <div id="adventure-carousel" class="adventure-carousel">
            <?php foreach( $images as $image ): ?>
                <div class="adventure-carousel-item">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="adventure-carousel-img"/>
                </div>
            <?php endforeach; ?>
          </div>
        <span class="adventure-carousel-right sketch-arrow"></span>
        <span class="adventure-carousel-left sketch-arrow flip"></span>
      </div>
    <?php endif; ?>
  </div>
  <div class="hidden-xs hidden-sm col-md-4 padding-clear">
    <?php get_template_part('parts/page-menu'); ?>
  </div>
  <div class="col-md-12">
    <?php get_template_part('parts/page-content'); ?>
  </div>
  <div class="col-md-12">
    <?php if (!empty(the_content())) { ?>
      <p>
        <?php get_template_part('parts/page-content'); ?>
      </p>
    <?php } ?>
  </div>
</div>
<div class="hidden-md hidden-lg">
  <div class="adventure-sub-menu-link-block">
    <a href="<?php the_field('link_block_link_after_menu'); ?>">
      <div class="image-wrapper">
        <img src="<?php the_field('link_block_image_after_menu'); ?>" alt="<?php the_field('link_block_title_after_menu'); ?>" class="adventure-sub-menu-after-bg-image ">
      </div>
      <div class="adventure-sub-menu-after-overlay">
        <h3 class="margin-clear adventure-sub-menu-title-text">
          <?php the_field('link_block_title_after_menu'); ?>
        </h3>
        <span class="adventure-sub-menu-link-text sketch-line-after">
          <?php the_field('link_block_link_title_after_menu'); ?>
        </span>
      </div>
    </a>
  </div>
</div>
<?php get_template_part('parts/feedback-block'); ?>

<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
