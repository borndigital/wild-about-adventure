<nav class="nav-primary">
  <div class="container wrap">
    <div class="hidden-xs hidden-sm col-md-9 padding-clear">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills']);
      endif;
      ?>
    </div>
    <div class="col-xs-7 col-sm-4 col-md-3 padding-clear">
      <div class="enquire-now-wrap">
        <div class="enquire-now-link" id="enquire-now-trigger">
          Enquire now about you're next adventure
        </div>
      </div>
    </div>
    <div class="col-xs-5 col-sm-8 visible-xs-block visible-sm-block text-right">
      <div class="mobile-menu-icon" id="toggle-mobile-menu"></div>
    </div>
  </div>
</nav>
<div class="hidden mobile-menu-wrap hidden-md hidden-lg">
  <?php
  if (has_nav_menu('primary_navigation')) :
    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'mobile-menu-nav']);
  endif;
  ?>
</div>
<?php get_template_part('parts/header-dropdown-enquire'); ?>
<?php get_template_part('parts/header-dropdown-adventure-categories'); ?>
<div class="main-wrapper">
  <header class="banner">
    <div class="container">
      <div class="brand-wrap">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>">
          <?php bloginfo('name'); ?>
        </a>
      </div>
    </div>
  </header>

