<?php the_content(); ?>
<div>
  <ul class="contact-list">
    <?php if ( ! empty( get_field('email') ) ) : ?>
      <li class="contact-item email">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-envelope fa-stack-1x"></i>
        </span>
        <a href="mailto:<?php the_field('email'); ?>?Subject=Hello" target="_top" class="alt-link">
          <?php the_field('email'); ?>
        </a>
      </li>
    <?php endif; ?>
    <?php if ( ! empty( get_field('mobile-phone') ) ) : ?>
      <li class="contact-item phone">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-phone fa-stack-1x"></i>
        </span>
        <a href="tel:+<?php the_field('mobile-phone'); ?>" class="alt-link"><?php the_field('mobile-phone'); ?></a> /
        <a href="tel:+<?php the_field('other-phone'); ?>" class="alt-link"><?php the_field('other-phone'); ?></a>
      </li>
    <?php endif; ?>
    <?php if ( ! empty( get_field('address') ) ) : ?>
      <li class="contact-item address">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-pencil fa-stack-1x"></i>
        </span>
        <?php the_field('address'); ?>
      </li>
    <?php endif; ?>
  </ul>
</div>

<div class="text-after">
  <?php the_field('text_after'); ?>
</div>

<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
