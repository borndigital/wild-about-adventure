<!-- Bookings modal -->
<?php $terms = get_terms("tribe_events_cat"); ?>
<div class="modal fade booking-modal" id="booking-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="hidden-xs">&times;</span>
          <h4 class="sticky visible-xs-block">Close Window</h4>
        </button>
      </div>
      <div class="modal-body">

        <div class="clearfix">
          <div class="modal-content-form hidden-xs col-sm-6 form-side">

            <div class="initial-bookings-form">
              <?php
                $id_or_title = 'Bookings';
                $display_title = true;
                $display_description = true;
                $display_inactive = false;
                $field_values = null;
                $ajax = true;
                $tabindex = 1;
                $echo = true;
                echo gravity_form($id_or_title, $display_title, $display_description, $display_inactive, $field_values, $ajax, $tabindex, $echo);
              ?>
            </div>

            <div class="event-info hidden">
              <!-- This will contain information about an event -->
            </div>

            <div class="event-form hidden">
              <!-- This will contain the booking form for an event -->
            </div>

          </div>

          <div class="modal-content-cal col-sm-6 events-side" id="events-side">
            <!-- The calendar -->
            <div class="inner">
              The content from the events page will show on this side
            </div>

            <div class="waa-event-filters">
               <ul class="row list-unstyled event-type-list">
                <?php foreach ($terms as $term) { ?>
                  <li class="event-type-item <?php echo $term->slug; ?> active" data-event-category="<?php echo $term->slug; ?>">
                      <?php echo $term->name; ?>
                  </li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <div class="modal-content-form visible-xs-block col-sm-6 form-side">

            <div class="initial-bookings-form">
              <?php
                $id_or_title = 'Bookings';
                $display_title = true;
                $display_description = true;
                $display_inactive = false;
                $field_values = null;
                $ajax = true;
                $tabindex = 1;
                $echo = true;
                echo gravity_form($id_or_title, $display_title, $display_description, $display_inactive, $field_values, $ajax, $tabindex, $echo);
              ?>
            </div>

            <div class="event-info hidden">
              <!-- This will contain information about an event -->
            </div>

            <div class="event-form hidden">
              <!-- This will contain the booking form for an event -->
            </div>

          </div>

        </div>

      </div>
    </div>
  </div>
</div>

<footer class="content-info footer-wrap">
  <div class="container">
    <?php wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav nav-pills']); ?>
  </div>
</footer>
