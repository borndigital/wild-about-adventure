<div class="row">
  <?php if (have_rows('instructors')): ?>
    <?php while (have_rows('instructors')) : the_row(); ?>
      <div class="col-sm-6">
        <div class="row">
          <div class="col-sm-12">
            <div class="instructor-name">
              <?php the_sub_field('name'); ?>
            </div>
          </div>
          <div class="col-sm-5">
            <div class="instructor-image">
              <img src="<?php the_sub_field('image') ?>" alt="<?php the_sub_field('title'); ?>" />
            </div>
          </div>
          <div class="col-sm-7">
            <div class="instructor-title">
              <?php the_sub_field('title'); ?>
            </div>
            <div class="instructor-description">
              <?php the_sub_field('description'); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</div>
