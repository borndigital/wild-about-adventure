<?php use Roots\Sage\Titles; ?>
<?php $page_templates = array( 'page-with-side-menu.php', 'page-with-inpage-menu.php' ); ?>
<div class="page-header">
  <h1><?= Titles\title(); ?></h1>
  <?php if( !is_page_template( $page_templates ) ) { ?>
    <a href="/" type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </a>
  <?php } ?>
</div>
